package com.umss.pivotal;

import org.testng.Assert;
import org.testng.annotations.*;

import static org.testng.Assert.*;

public class CalcTest {
    //@BeforeTest //muestra imprimr clase de test
    //@BeforeMethod//muestra imprimir metodos
    @BeforeClass
    public void setUp(){
        System.out.println("Login");
        System.out.println("Create Project");
    }
    @org.testng.annotations.Test
    public void testSuma() {
        int valorEsperado = 15;
        Assert.assertEquals(Calc.suma(8, 7),valorEsperado);
        Assert.assertEquals(Calc.suma(10, 5),valorEsperado);
        Assert.assertEquals(Calc.suma(14, 1),valorEsperado);
    }

    @org.testng.annotations.Test
    public void testResta() {
        Assert.assertEquals(Calc.resta(14, 1),13);
        Assert.assertEquals(Calc.resta(10, 9),1);
        Assert.assertEquals(Calc.resta(8, 8),0);
        Assert.assertEquals(Calc.resta(8, 9),-1);
    }

    @org.testng.annotations.Test
    public void testMultiplicar() {
        Assert.assertEquals(Calc.multiplicar(5, 5),25);
    }

    @org.testng.annotations.Test
    public void testDividir() {
        Assert.assertEquals(Calc.dividir(10, 2),5);
    }
    @AfterClass
    public void cleanUp(){
        System.out.println("Logout");
    }
}