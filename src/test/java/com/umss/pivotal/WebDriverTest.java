package com.umss.pivotal;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class WebDriverTest {
    private WebDriver driver;

    @Test
    public void testGoogleSearchChrome(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.google.com/");
        driver.findElement(By.cssSelector("input[title='Buscar']")).clear();
        driver.findElement(By.cssSelector("input[title='Buscar']")).sendKeys("Test"+ Keys.ENTER);
        String expectedText = "Speedtest por Ookla - La prueba de velocidad de banda ...";

        //String expectedText = "hola mundo";

        String actualText =driver.findElement(By.xpath("(//h3//span)[1]")).getText();

        Assert.assertEquals(expectedText,actualText);
    }

    @Test
    public void testGoogleSearchFirefox(){
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://www.google.com/");
        driver.findElement(By.cssSelector("input[title='Buscar']")).clear();
        driver.findElement(By.cssSelector("input[title='Buscar']")).sendKeys("Test");
        driver.findElement(By.cssSelector("input[title='Buscar']")).sendKeys(Keys.ENTER);
        String expectedText = "Speedtest por Ookla - La prueba de velocidad de banda ...";

        //String expectedText = "hola mundo";

        String actualText =driver.findElement(By.xpath("(//h3//span)[1]")).getText();

        Assert.assertEquals(expectedText,actualText);
    }
    @Test
    public void testPivotalCreateProject(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.pivotaltracker.com/");
        driver.findElement(By.xpath("(//a[@class='header__link header__link-signin'])[2]")).click();
        driver.findElement(By.id("credentials_username")).sendKeys("rodri.ledezma9483@gmail.com");
        driver.findElement(By.cssSelector("input[value='NEXT']")).click();
        driver.findElement(By.id("credentials_password")).sendKeys("Rodri03071994");
        driver.findElement(By.cssSelector("input[value='SIGN IN']")).click();
        String expected = "RODRIGOARIEL";
        String actual = driver.findElement(By.cssSelector("button[aria-label='Profile Dropdown']")).getText();

        Assert.assertEquals(actual,expected);
    }

    @AfterClass
    public void cleanUp(){
        driver.quit();
    }
}
