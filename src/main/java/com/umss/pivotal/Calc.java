package com.umss.pivotal;

public class Calc {
    public static int suma(final int numA, final int numB){
        return numA+numB;
    }
    public static int resta(final int numA, final int numB){
        return numA-numB;
    }
    public static int multiplicar(final int numA, final int numB){
        return numA*numB;
    }
    public static int dividir(final int numA, final int numB){
        return numA/numB;
    }
}
